#!/usr/bin/env stack
-- stack --install-ghc runghc --package turtle

{-# LANGUAGE OverloadedStrings #-}

import Turtle
import Data.Time
import Data.Text
import Prelude hiding (FilePath)

parser :: Parser (FilePath)
parser = argPath "src" "The source file"


flaggedName :: FilePath -> Day -> FilePath
flaggedName fqn rmDate = let dir = directory fqn
                             name = filename fqn
                             rmFlag = "rm"
                             dName = dir </> name in
                             dName <.> (pack $ show rmDate) <.> rmFlag

thirtyDaysFromNow :: IO Day
thirtyDaysFromNow = (addDays 30 . utctDay) <$> getCurrentTime

main = do
    (src) <- options "A simple `cp` script " parser
    rmDate <- thirtyDaysFromNow
    print $ flaggedName src rmDate
    mv src (flaggedName src rmDate)
