# flagRm
This is a haskell version of flagRm.

## Requirements
You will need to install stack and turtle

stack https://docs.haskellstack.org/en/stable/README/

turtle https://hackage.haskell.org/package/turtle


```shell
stack install turtle
```

If you delete the first two lines of the program, you can also compile the script.
```
$ # `-O2` turns on all optimizations
$ # `-threaded` helps with piping shell output in and out of Haskell
$ stack ghc -- -O2 -threaded flagRm.hs
```

## Usage
```
flagRm aFile

aFile.20170913.rm
```
