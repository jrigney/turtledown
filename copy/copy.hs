#!/usr/bin/env stack
-- stack --install-ghc runghc --package turtle

{-# LANGUAGE OverloadedStrings #-}

import Turtle
import Data.Time
import Data.Text hiding (empty)
import Prelude hiding (FilePath)
import System.Info  (os, arch)

parser :: Parser (FilePath)
parser = argPath "src" "The source file"


flaggedName :: FilePath -> Day -> FilePath
flaggedName fqn rmDate = let dir = directory fqn
                             name = filename fqn
                             rmFlag = "rm"
                             dName = dir </> name in
                             dName <.> (pack $ show rmDate) <.> rmFlag

thirtyDaysFromNow :: IO Day
thirtyDaysFromNow = (addDays 30 . utctDay) <$> getCurrentTime

darwinMd5Cmd :: FilePath -> String
darwinMd5Cmd fqn = "find " ++ (unpack (format fp fqn)) ++ " -type f -exec md5 -r {} +"

linuxMd5Cmd :: FilePath -> String
linuxMd5Cmd fqn = "find " ++ (unpack (format fp fqn)) ++ " -type f -exec md5sum {} +"

osMd5Cmd :: String -> FilePath -> String
osMd5Cmd "darwin" fqn = darwinMd5Cmd fqn
osMd5Cmd "linux" fqn = linuxMd5Cmd fqn

main = do
    (src) <- options "A simple `cp` script " parser
    print (format fp src)
    print (osMd5Cmd os src)
    print [os,arch]
    shell (pack (osMd5Cmd os src)) empty
